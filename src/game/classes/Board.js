class Board {

  constructor() {
    this.matrix = [
      [' ', ' ', ' '],
      [' ', ' ', ' '],
      [' ', ' ', ' '],
    ]
  }

  setCell(mark, x, y) {
    if (this.matrix[x - 1][y - 1] === ' ') {
      this.matrix[x - 1][y - 1] = mark
      return false
    } else {
      return true
    }
  }

  getMatrix() {
    return this.matrix
  }

  printMatrix() {
    for (let i = 0; i < 3; i++) {
      console.log(this.matrix[i][0], '|', this.matrix[i][1], '|', this.matrix[i][2])
      if (i < 2) console.log('---------')
    }
  }
}

module.exports = Board