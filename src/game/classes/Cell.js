class Cell {
  constructor() {
    this.mark = ''
    this.x = ''
    this.y = ''
  }

  setValue(mark, position) {
    if (!position) {
      console.log('\x1b[41m', 'You did not enter an answer', '\x1b[0m')
      return
    }

    if (position.indexOf('-') === -1) {
      console.log('\x1b[41m', 'Incorrect data format', '\x1b[0m')
      return
    }

    const value = position.split('-')
    if (!/^[1-3]$/.test(value[0]) || !/^[1-3]$/.test(value[1])) {
      console.log('\x1b[41m', 'The entered data is not correct', '\x1b[0m')
      return
    }
    this.mark = mark
    const posArr = position.split('-')
    this.x = posArr[0]
    this.y = posArr[1]
  }

}

module.exports = Cell