const readline = require("readline")
const { NAME, MARK, X, O } = require("../constants/constants")
const Player = require('./Player')
const Cell = require('./Cell')
const ReferenceBoard = require("./ReferenceBoard")
const Board = require('./Board')

class Game {

  constructor() {
    this.player1 = {}
    this.player2 = {}
    this.cell = new Cell
    this.board = {}
    this.referenceBoard = {}
    this.gameStart()
  }

  async gameStart() {

    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    console.clear()

    // ==================== Creating players  ========================
    const questionPlayerCreator = (questionText) => {
      return new Promise((resolve, reject) => {
        rl.question(questionText, (answer) => {
          resolve(answer)
        })
      })
    }

    const playerCreator = () => {
      return new Promise((resolve, reject) => {
        const startPlayerCreator = async () => {
          while (!this.player1.name) {
            const answer = await questionPlayerCreator("First player name? ", NAME)
            this.player1 = new Player(answer)
          }

          while (!this.player1.mark) {
            const answer = await questionPlayerCreator(`First player mark "X" or "O"? `, MARK)
            this.player1.setMark(answer)
          }

          while (!this.player2.name) {
            const answer = await questionPlayerCreator("Second player name? ", NAME)
            this.player2 = new Player(answer)
          }

          this.player2.setMark(this.player1.mark === X ? O : X)
          resolve()
        }
        startPlayerCreator()
      })

    }
    await playerCreator()

    // ==================== Creating cells ========================
    const cellCreatorPromise = (currentPlayer) => {
      return new Promise((resolve, reject) => {
        rl.question(`Now it's ${currentPlayer} turn. For example "1-2": `, (position) => {
          resolve(position)
        })
      })
    }

    const cellCreatorStart = (currentPlayer, mark) => {
      return new Promise((resolve, reject) => {
        const cellCreator = async () => {
          this.cell = new Cell()
          while (!this.cell.mark) {
            const position = await cellCreatorPromise(currentPlayer)
            this.cell.setValue(mark, position)
          }
          resolve()
        }
        cellCreator()
      })
    }

    this.board = new Board()

    let cellBusy

    //==================== Start making moves ========================
    for (let i = 2; i <= 10; i++) {

      // === Determine the current player ===
      const mark = i % 2 ? O : X
      let currentPlayer
      if (mark === this.player1.mark) currentPlayer = this.player1.name
      else currentPlayer = this.player2.name

      //=== Displaying the board ===
      console.clear()
      console.log(`${this.player1.name} with "${this.player1.mark}"`)
      console.log(`${this.player2.name} with "${this.player2.mark}" \n`)
      this.board.printMatrix()
      console.log("\n")

      if (cellBusy) console.log('\x1b[41m', "Cell is busy", '\x1b[0m')

      // ======= Creating cell =====
      await cellCreatorStart(currentPlayer, mark)
      cellBusy = this.board.setCell(this.cell.mark, this.cell.x, this.cell.y)
      if (cellBusy) i--
      else {
        this.referenceBoard = new ReferenceBoard(mark)

        const theEnd = this.referenceBoard.checkResult(this.board.getMatrix(), currentPlayer)

        if (theEnd) {
          console.clear()
          console.log(theEnd)    // Displaying the winner
          rl.close()
          break
        }
        if (i === 10) {
          console.clear()
          console.log("No winner ")   // Displaying game over
          rl.close()
        }
      }
    }
  }
}

module.exports = Game