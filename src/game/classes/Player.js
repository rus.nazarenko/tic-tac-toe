const { X, O } = require("../constants/constants")

class Player {
  constructor(name, mark) {
    this.name = ''
    this.mark = ''
    const validatorName = (name) => {
      if (!name) {
        console.log('\x1b[41m', 'You did not enter an answer', '\x1b[0m')
        return
      }
      if (!/^[a-z]+$/i.test(name)) {
        console.log('\x1b[41m', 'Only letters need to be entered', '\x1b[0m')
        return
      }
      this.name = name
    }
    validatorName(name)
  }

  setMark(mark) {
    if (!mark) {
      console.log('\x1b[41m', 'You did not enter an answer', '\x1b[0m')
      return
    }
    if (mark !== X && mark !== O) {
      console.log('\x1b[41m', 'You need to enter "X" or "O"', '\x1b[0m')
      return
    }
    this.mark = mark
  }
}

module.exports = Player