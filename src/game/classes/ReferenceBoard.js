class ReferenceBoard {
  constructor(mark) {
    this.matrix1 = [[mark, mark, mark]]
    this.matrix2 = [[mark, '_', '_'], [mark, '_', '_'], [mark, ' ', ' ']]
    this.matrix3 = [['_', mark, '_'], ['_', mark, '_'], [' ', mark, ' ']]
    this.matrix4 = [['_', '_', mark], ['_', '_', mark], [' ', ' ', mark]]
    this.matrix5 = [[mark, '_', '_'], ['_', mark, '_'], [' ', ' ', mark]]
    this.matrix6 = [['_', '_', mark], ['_', mark, '_'], [mark, ' ', ' ']]
  }

  checkResult(matrix, currentPlayer) {
    if (JSON.stringify(this.matrix1[0]) === JSON.stringify(matrix[0])) return `\n${currentPlayer} won!!!`
    if (JSON.stringify(this.matrix1[0]) === JSON.stringify(matrix[1])) return `\n${currentPlayer} won!!!`
    if (JSON.stringify(this.matrix1[0]) === JSON.stringify(matrix[2])) return `\n${currentPlayer} won!!!`
    if (
      JSON.stringify(`${matrix[0][0]}, ${matrix[1][0]}, ${matrix[2][0]}`) === JSON.stringify(`${this.matrix2[0][0]}, ${this.matrix2[1][0]}, ${this.matrix2[2][0]}`)
    ) return `\n${currentPlayer} won!!!`
    if (
      JSON.stringify(`${matrix[0][1]}, ${matrix[1][1]}, ${matrix[2][1]}`) === JSON.stringify(`${this.matrix3[0][1]}, ${this.matrix3[1][1]}, ${this.matrix3[2][1]}`)
    ) return `\n${currentPlayer} won!!!`
    if (
      JSON.stringify(`${matrix[0][2]}, ${matrix[1][2]}, ${matrix[2][2]}`) === JSON.stringify(`${this.matrix4[0][2]}, ${this.matrix4[1][2]}, ${this.matrix4[2][2]}`)
    ) return `\n${currentPlayer} won!!!`
    if (
      JSON.stringify(`${matrix[0][0]}, ${matrix[1][1]}, ${matrix[2][2]}`) === JSON.stringify(`${this.matrix5[0][0]}, ${this.matrix5[1][1]}, ${this.matrix5[2][2]}`)
    ) return `\n${currentPlayer} won!!!`
    if (
      JSON.stringify(`${matrix[0][2]}, ${matrix[1][1]}, ${matrix[2][0]}`) === JSON.stringify(`${this.matrix6[0][2]}, ${this.matrix6[1][1]}, ${this.matrix6[2][0]}`)
    ) return `\n${currentPlayer} won!!!`

    return false
  }
}

module.exports = ReferenceBoard